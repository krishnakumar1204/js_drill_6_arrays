function flatten(elements) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

    if (Array.isArray(elements)) {

        let flattenedArray = [];

        for (let index = 0; index < elements.length; index++) {

            if (Array.isArray(elements[index])) {
                flattenedArray = flattenedArray.concat(flatten(elements[index]));
            }
            else {
                flattenedArray.push(elements[index])
            }
        }

        return flattenedArray;
    }
    else {
        console.log("input should be a valid Array");
    }
}

Array.prototype.flatten = flatten;

module.exports = flatten;