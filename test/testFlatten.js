const flatten = require("../flatten");

const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

try {

    let flattenedArray = flatten(nestedArray);
    
    if (Array.isArray(flattenedArray)) {
        console.log(flattenedArray);
    }

} catch (error) {
    console.log("Something went wrong");
}