const items = require("../data");
const map = require("../map");


try {

    let fiveTimesArray = items.map(items, (item) => 5 * item);

    if (Array.isArray(fiveTimesArray)) {
        
        console.log(fiveTimesArray);
    }

} catch (error) {

    console.log("Something went wrong");
}