const items = require("../data");
const reduce = require("../reduce");


try {

    let sumOfItems = items.reduce(items, (sum, item) => sum + item, 0);
    if (sumOfItems != undefined) {
        console.log(sumOfItems);
    }

} catch (error) {

    console.log("Something went wrong");
}