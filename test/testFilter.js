const items = require("../data");
const filter = require("../filter");

try {

    let oddItems = items.filter(items, (item) => {
        return item % 2;
    });

    if (Array.isArray(oddItems)) {
        console.log(oddItems);
    }

} catch (error) {
    console.log("Something went wrong");
}