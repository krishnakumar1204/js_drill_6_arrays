function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test


    if (Array.isArray(elements)) {

        let resultArray = [];

        for (let index = 0; index < elements.length; index++) {
            
            if (cb(elements[index])) {
                
                resultArray.push(elements[index]);
            }
        }

        return resultArray;
    }
    else{
        console.log("Input should be a valid Array");
    }
}

Array.prototype.filter = filter;

module.exports = filter;